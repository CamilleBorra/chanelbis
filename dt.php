<!DOCTYPE html>
<?php require_once "bdd.php"; ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dt</title>
    <link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" media="all" href="style/style.css" type="text/css">
    <script src="style/jquery.js"></script>
</head>
<body>
    <div class="container mt-3">
        <h1>Logs</h1>
        <form data-dashlane-rid="5fe9a48df519ee10" data-form-type="other">
            <fieldset>
                <div class="form-group">
                    <label for="exampleSelect1" class="form-label mt-4">Sélectionnez un dépôt</label>
                    <select onchange="window.location.href='dt.php?depot=' + $(this).val()" class="form-select">
                        <option>Choisissez un dépôt</option>
                        <?php

                            $getDepots = $db->query('SELECT depot FROM Gan GROUP BY depot');

                            if($getDepots->rowCount() !== 0) {
                                $fetchDepots = $getDepots->fetchAll(PDO::FETCH_ASSOC);

                                for ($i = 0; $i < count($fetchDepots); $i++) { 
                                    $depot = $fetchDepots[$i];

                                    if(isset($_GET['depot'])) {
                                        if($_GET['depot'] === $depot['depot']) {
                                            echo '<option selected="selected" value="' . $depot['depot'] . '">' . $depot['depot'] . '</option>';
                                        } else {
                                            echo '<option value="' . $depot['depot'] . '">' . $depot['depot'] . '</option>';
                                        }
                                    } else {
                                        echo '<option value="' . $depot['depot'] . '">' . $depot['depot'] . '</option>';
                                    }
                                    
                                }
                            }

                        ?>
                    </select>

                    <?php if(isset($_GET['depot'])): ?>
                        <div class="table-responsive mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">VILLE</th>
                                        <th scope="col">TYPE</th>
                                        <th scope="col">MOB</th>
                                        <th scope="col">AFFICHEUR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                        $getAll = $db->query('SELECT `ville`, `type`, `mob`, `afficheur` FROM `Gan` WHERE `depot` = "' . $_GET['depot'] . '" AND `flag` = 0 ORDER BY `ville` ASC');

                                        if($getAll->rowCount() !== 0): ?>
                                            <?php
                                            
                                                $fetchAll = $getAll->fetchAll(PDO::FETCH_ASSOC); 

                                                for ($i=0; $i < count($fetchAll); $i++) { 
                                                    $ligne = $fetchAll[$i];

                                                    ?>

                                                        <tr>
                                                            <td><?= $ligne['ville']; ?></td>
                                                            <td><?= $ligne['type']; ?></td>
                                                            <td><?= $ligne['mob']; ?></td>
                                                            <td><?= $ligne['afficheur']; ?></td>
                                                        </tr>

                                                    <?php
                                                }

                                            ?>
                                        <?php else: ?>
                                            <tr>
                                                <td>#</td>
                                                <td>#</td>
                                                <td>#</td>
                                                <td>#</td>
                                            </tr>
                                        <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif ?>
                </div>
            </fieldset>
        </form>
    </div>
</body>
</html>
