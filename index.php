<?php
ini_set('display_errors', 'On');
require_once "bdd.php";
$exist=0;
$periode=$db->query("SELECT periode FROM chanel");
while($periodeExe=$periode->fetch())
{
    
    $now=new DateTime();
    $periodee=new DateTime($periodeExe['periode']);
    $diff=$now->diff($periodee);
    if($now>$periodee){
        if($diff->d>7)
        {
            $exist=1;
        }
    }
    else 
    {
        if($diff->d>31)
        {
            $exist=1;
        }
    }
    
    
}


    if(isset($_GET['num'])){
    $afficheur=$_GET['num'];

    
    if(isset($_GET['face'])){
        $faceChoisis=$_GET['face'];
        $visuel=$db->query("SELECT nomDuVisuel FROM chanel WHERE face=\"$faceChoisis\"");
    }
    }
    
    
    




?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" media="all" href="style/style.css" type="text/css">
    <title>Document</title>
</head>

<body>
    <?php if($exist==0): ?>
    <div class="container">
        <form method="POST" action="insert.php" enctype="multipart/form-data">
            <?php if(!isset($_GET['num'])||empty($_GET['num'])):?>
            <div class="form-group">
                <label for="num" class="form-label mt-4">Afficheur</label>
                <input type="text" class="form-control" id="num" name="num" placeholder="afficheur" value="ROL75">
            </div>
            <button id="suivant" type="button">Suivant</button>
            <?php endif ?>
            <?php if(isset($_GET['num'])&&!empty($_GET['num'])):
                $afficheur = $_GET['num'];
                if($afficheur=='ROL75'){
                    $face = $db->query("SELECT mob, ville, flag, type FROM chanel WHERE flag=0  ORDER BY ville,mob");
                }
                else{
                    $face = $db->query("SELECT mob, ville, flag, type FROM chanel WHERE afficheur=\"$afficheur\" AND flag=0  ORDER BY ville,mob");
                }
            ?>
                <div class="form-group">
                    <label for="num" class="form-label mt-4">Afficheur</label>
                    <input type="text" class="form-control" id="num" name="num" value=<?=$_GET['num']?>>
                </div>
                <div class="form-group " id="faces">
                    <label for="face" class="form-label mt-4">Faces</label>
                    <select class="form-select" id="face" name="face">
                        <?php if(isset($_GET['face'])):?>
                            <option value=<?=$_GET['face']?>><?=$_GET['face']?></option>
                        <?php else: ?>
                            <option value="default"></option>
                        <?php endif;?>
                        <?php while($faceExe=$face->fetch()):?>
                            
                            <option value=<?=$faceExe['ville'].'_'.$faceExe['type'].'_'.$faceExe['mob']?>><?=$faceExe['ville'].'_'.$faceExe['type'].'_'.$faceExe['mob']?></option>
                        
                       		 
                        <?php endwhile;?>
                    </select>
                </div>
                <?php if(isset($_GET['face'])&&!empty($_GET['face'])):
                    $faceChoisisExplode = explode('_',$_GET['face']);
                    $faceChoisis        = $faceChoisisExplode[2];
                    $VilleChoisis       = $faceChoisisExplode[0];
//bdp
                     $typechoisis        = $faceChoisisExplode[1];
                 //   var_dump($typechoisis);
                    $visuel = $db->query("SELECT nomDuVisuel, idDuVisuel FROM chanel WHERE mob=\"$faceChoisis\" AND ville=\"$VilleChoisis\" AND type=\"$typechoisis\"");

                  //  $visuel = $db->query("SELECT nomDuVisuel, idDuVisuel FROM chanel WHERE mob=\"$faceChoisis\" AND ville=\"$VilleChoisis\"");
                    
                /*    while($visuelExe=$visuel->fetch()):
                        $namePdf = $visuelExe['idDuVisuel'];
                        $explode = explode(".",$namePdf);
                        $tabPdf=scandir('pdf/');
                        

                        if (in_array($explode[0]." ok", $tabPdf)) {


                            $namePdf = $explode[0]." ok";
                        } elseif (in_array($explode[0]." OK", $tabPdf)) {
                            $namePdf = $explode[0]." OK";
                        }
			  else {
			    $namePdf = $explode[0];	
			}

                        //var_dump($explode[0]);
                        //var_dump(in_array($explode[0]." ok", $tabPdf));

                        $pdf = scandir("pdf/$namePdf");
                        //var_dump($pdf);
                    endwhile;*/
                
                ?>
 
                      
                        <div id="form-camera" class="form-group ">
                            <label for="images">Prendre une photo</label>
                            <input type="file" id="images" name="images"  required="required" capture>
                        </div>
			<div>
				<input type="checkbox" id="check" name="check">
				<label for="check">Ok</label>
			</div>
			<div class="form-group"><label for="commentaire" class="form-label mt-4"> Commentaire</label><textarea name="commentaire" class="form-control" id="commentaire" rows="3" data-form-type="other"></textarea></div>
                        <input type="submit" name="submit" id="submitButton" value="valider" class="btn btn-success">
                   
                <?php endif?>
            <?php endif;?>
        </form>
    </div>
    <?php else: ?>
    <p>Page Introuvable</p>
    <?php endif; ?>

    <script src="style/jquery.js"></script>
    <script>
        $(document).ready(function () {
            $('#suivant').click(function () {
                window.location.href = `?num=${document.getElementById('num').value}`
            })

            $('#face').click(function (){
                <?php if (isset($_GET['face'])&& $_GET['face']!=="defaults"):?>
                    alert("Vous n'avez pas fait de photo de la face précédente")
                    window.location.href = `?num=${document.getElementById('num').value}`
                <?php endif ?>
            })

            $('#face').change(function () {
                window.location.href = `?num=${document.getElementById('num').value}&face=${document.getElementById('face').value}`
            })

            $('#num').blur(function () {
                window.location.href = `?num=${document.getElementById('num').value}`
            })
        })
    </script>
</body>
</html>
